#!/usr/bin/env python

import numpy as np

from pycbc import frame
import lalsimulation as ls

import gw_reconstruct as gwr

from matplotlib import pyplot as plt

# Load the data
ifos = ['H1', 'L1']

cache = './{}.cache'
channel = '{}:GDS-CALIB_STRAIN'

trigger_time = 1126259462.39

sample_rate = 4096.
segment_length = 8.
time_end = trigger_time + 2
time_start = time_end - segment_length
flow, fhigh = 30., 400.

data = [gwr.Strain(frame.read_frame(cache.format(ifo), channel.format(ifo), time_start, time_end)) for ifo in ifos]
data = [gwr.resample(strain, sample_rate) for strain in data]

# Load PSDs for whitening
bayesline_psd = 'bl_psd.dat.{}'
infiles = [bayesline_psd.format(gwr.BW_IFOS[ifo]) for ifo in ifos]
psds = [gwr.psd.interp_bayesline_from_txt(infile, flow=flow, fhigh=fhigh) for infile in infiles]

# Make some white data
white_data = [gwr.whiten(d, ipsd) for d, ipsd in zip(data, psds)]

# Load ~100 posterior samples
ndraws = 100
infile = './posterior_samples.dat'
pos = gwr.posterior.extract_samples(infile)
pos = pos[::int(len(pos)/ndraws)]

# Generate waveforms
strains = [gwr.generate_strain_from_sample(samp, sample_rate=sample_rate, duration=segment_length, epoch=time_start) for samp in pos]

# Make the plots
fig, axs = plt.subplots(1, 2, figsize=(12, 4))

cl = 0.95
alpha = 0.4

for ifo, ax in enumerate(axs):
    ax.plot(white_data[ifo].times, white_data[ifo], lw=0.2, color='k', alpha=0.5)

for ifo, ax in enumerate(axs):
    pos_ht = np.zeros((len(strains), len(white_data[ifo])))
    for wave, ht in enumerate(strains):
        pos_ht[wave] = gwr.whiten(ht[ifo], psds[ifo])

    lower_cl = np.sort(pos_ht, axis=0)[int((1-cl)/2.*len(pos_ht)), :]
    upper_cl = np.sort(pos_ht, axis=0)[int((1+cl)/2.*len(pos_ht)), :]
    ax.fill_between(white_data[ifo].times, lower_cl, upper_cl, alpha=alpha)

axs[0].set_ylabel('whitened $h(t)$')

for ax, ifo in zip(axs, ifos):
    ax.set_title(ifo)
    ax.set_xlabel('time (s)')
    ax.set_xlim(trigger_time - .15, trigger_time + .1)
plt.show()
