# gw-reconstruct

This package is designed to facilitate the visualization and assessment of
signal reconstructions from CBC and Burst followup analyses.

## Dependencies

This package requires numpy, scipy, and [pycbc](http://ligo-cbc.github.io/pycbc/latest/html/install.html).

## Example usage

```python
from pycbc import frame
import lalsimulation as ls

import gw_reconstruct as gwr

from matplotlib import pyplot as plt

# Load the data
ifos = ['H1', 'L1']

cache = './{}.cache'
channel = '{}:GDS-CALIB_STRAIN'

trigger_time = 1126259462.39

segment_length = 8.
time_end = trigger_time + 2
time_start = time_end - segment_length
flow = 16.

data = [gwr.Strain(frame.read_frame(cache.format(ifo), channel.format(ifo), time_start, time_end)) for ifo in ifos]

# Load PSDs for whitening
psd_infile_fmt = '{}_psd.dat'
infiles = [psd_infile_fmt.format(ifo) for ifo in ifos]
psds = [gwr.psd.interp_from_txt(infile, flow=flow) for infile in infiles]

# Whiten the data
white_data = [gwr.whiten_strain(d, psd) for d, psd in zip(data, psds)]

# Load "best fit" sample from LALInference and generate strains
infile = './posterior_samples.dat'
map_params = gwr.posterior.extract_map_sample(infile)
map_hts = gwr.generate_strain_from_sample(map_params, duration=segment_length, epoch=time_start)

# Make residuals
residuals = [td - h for td, h in zip(data, map_hts)]
white_residuals = [gwr.whiten_strain(d, ipsd) for d, ipsd in zip(residuals, psds)]

# Plot spectrograms
fig, [axs1, axs2] = plt.subplots(2, 2, figsize=(12, 8))

gwr.plot.specgrams(white_data, ymin=flow, ymax=512, axs=axs1)
gwr.plot.specgrams(white_residuals, ymin=flow, ymax=512, axs=axs2)
for axs in [axs1, axs2]:
    for ax in axs:
        ax.set_xlim(trigger_time - 1., trigger_time + 1.)
plt.show()
```
