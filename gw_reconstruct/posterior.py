#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Posterior parsing routines.
"""

from __future__ import print_function

import numpy as np

def loglike(freqs, hs, ds, psds, flow=0.):
    logl = 0.

    for h, d, psd in zip(hs, ds, psds):
        df = freqs[1] - freqs[0]
        sel = freqs > flow

        hh = np.sum(4.0 * df * (h[sel].conj()*h[sel]).real/psd[sel])
        dh = np.sum(4.0 * df * (d[sel].conj()*h[sel]).real/psd[sel])
        dd = np.sum(4.0 * df * (d[sel].conj()*d[sel]).real/psd[sel])

        logl += -0.5*(hh - 2.0*dh)
        #logl -= np.sum(np.log(2.0*np.pi*psd/(4.0*df)))

    return logl

def parse_header(inp):
    """
    Get past the header of an output file and return column names
    """
    # Check if the header is more complex than just a line of column names
    check_nlines = 15

    simple_header = True
    header = inp.readline().split()
    if header[0] == "#":
        header = header[1:]
    ncol = len(header)
    nlines_read = 0
    while nlines_read < check_nlines:
        nlines_read += 1
        if len(inp.readline().split()) != ncol:
            simple_header = False
            break

    inp.seek(0)
    header = inp.readline().split()
    if header[0] == "#":
        header = header[1:]

    if not simple_header:
        while True:
            if len(header) > 0 and header[0] == 'cycle':
                break
            header = inp.readline().split()
    return [p.lower() for p in header]

def get_logpost(infile):
    """
    Extract the log-posterior series of the chain.
    """
    with open(infile, 'r') as inp:
        header = parse_header(inp)
        try:
            post_col = header.index('logpost')
            post = np.genfromtxt(inp, usecols=(post_col))
        except ValueError:
            try:
                prior_col = header.index('logprior')
                like_col = header.index('logl')
                logprior, loglike = np.genfromtxt(inp, usecols=(prior_col, like_col), unpack=True)
                post = logprior + loglike
            except ValueError:
                post_col = header.index('post')
                post = np.log(np.genfromtxt(inp, usecols=(post_col)))
    return post

def get_loglike(infile):
    """
    Extract the log-likelihood series of the chain.
    """
    with open(infile, 'r') as inp:
        header = parse_header(inp)
        like_col = header.index('logl')
        like = np.genfromtxt(inp, usecols=(like_col))
    return like

def extract_samples(infile, params=None):
    """
    Extract samples from a PTMCMC output file.
    """
    with open(infile, 'r') as inp:
        header = parse_header(inp)
        params = header if params is None else params
        samples = np.genfromtxt(inp, dtype=[(param, np.float) for param in params])

    return samples

def extract_map_sample(infile, params=None):
    """
    Extract the maximum a posteriori sample from a PTMCMC output file.
    """
    logpost = get_logpost(infile)
    map_idx = logpost.argmax()

    samples = extract_samples(infile, params)

    return samples[map_idx]

def extract_maxl_sample(infile, params=None):
    """
    Extract the maximum a posteriori sample from a PTMCMC output file.
    """
    loglike = get_loglike(infile)
    map_idx = loglike.argmax()

    samples = extract_samples(infile, params)

    return samples[map_idx]

def credible_bounds(function_samples, cl=0.90):
    """
    Get the upper and lower credible boundaries of a 1-D function from a sample.
    """
    function_samples = np.atleast_2d(function_samples)
    N = function_samples.shape[0]

    sorted_samples = np.sort(function_samples, axis=0)
    low = sorted_samples[int((1-cl)/2.*N), :]
    high = sorted_samples[int((1+cl)/2.*N), :]
    return low, high
